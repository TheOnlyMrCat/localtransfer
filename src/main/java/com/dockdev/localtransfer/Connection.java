package com.dockdev.localtransfer;

import java.io.*;
import java.net.Socket;
import java.util.Collection;
import java.util.HashSet;
import java.util.function.Supplier;

public class Connection implements AutoCloseable {

	private Socket socketConnection;
	private MessageManager manager;

	private Thread daemon;
	private boolean keepAlive;
	private boolean readClear = true;

	private BufferedReader is;
	private PrintWriter os;

	public Connection(String host, int timeout) throws IOException {
		this(() -> {
			try {
				return new Socket(host, Message.CONNECTION_PORT);
			} catch (IOException e) {
				throw new UncheckedIOException(e);
			}
		}, timeout);
	}

	public Connection(Supplier<? extends Socket> socketSupplier, int timeout) throws IOException {
		socketConnection = socketSupplier.get();
		socketConnection.setSoTimeout(timeout);

		is = new BufferedReader(new InputStreamReader(socketConnection.getInputStream()));
		os = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socketConnection.getOutputStream())));

		daemon = new Thread(() -> {
			while (keepAlive) {
				if (readClear) {
					try {
						manager.handleMessage(this, receiveMessage());
					} catch (IOException e) {
						manager.handleException(this, e);
					}
				}
			}
		});
	}

	public void sendMessage(Message message) throws IOException {
		os.print("LTBegin ");
		os.print(manager.getTypeName(message.getType()));
		os.println();

		os.println(message.getContent());

		os.print("LTEnd ");
		os.println(message.getChecksum());
		if (os.checkError()) throw new IOException("Error in print writer.");
	}

	public Message receiveMessage() throws IOException {
		readClear = false;
		String[] header = is.readLine().split(" ");
		if (header[0].equals("LTBegin")) {
			int type;
			try {
				type = manager.getTypeValue(header[1]);
			} catch (IllegalArgumentException e) {
				throw new LocalMessageException("Illegal message header: " + header[1], e);
			}
			Collection<String> body = new HashSet<>();

			String line = is.readLine();
			while (!line.startsWith("LTEnd")) {
				body.add(line);
				line = is.readLine();
			}
			int checksum = Integer.valueOf(line.split(" ")[1]);
			readClear = true;
			return new Message(type, String.join("\n", body), checksum);
		}
		readClear = true;
		return null;
	}

	public void close() throws Exception {
		keepAlive = false;
		daemon.join();

		socketConnection.close();
	}
}
