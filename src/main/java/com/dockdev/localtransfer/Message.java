package com.dockdev.localtransfer;

import com.google.common.primitives.Ints;

public class Message {

	public static final int CONNECTION_PORT = 9001;

	private int type;
	private String content;
	private int checksum;

	Message(int type, String content) {
		this.type = type;
		this.content = content;
		checksum = calculateChecksum();
	}

	Message(int type, String content, int expectedChecksum) throws LocalMessageException {
		this.type = type;
		this.content = content;

		checksum = expectedChecksum;
		validateChecksum();
	}

	/**
	 * A simple checksum which XORs all the relevant bytes together.
	 * @return A single byte checksum for this message
	 */
	private byte calculateChecksum() {
		byte[] btype = Ints.toByteArray(type);
		byte[] bmessage = content.getBytes();

		byte chksum = 0;
		for (byte b : btype) {
			chksum ^= b;
		}

		for (byte b : bmessage) {
			chksum ^= b;
		}

		return chksum;
	}

	private void validateChecksum() throws LocalMessageException {
		if (calculateChecksum() != checksum) throw new LocalMessageException("Checksum validation failed");
	}

	public int getType() {
		return type;
	}

	public String getContent() {
		return content;
	}

	public int getChecksum() {
		return checksum;
	}
}
