package com.dockdev.localtransfer;

public interface MessageManager {

	/**
	 * Gets the name of the type represented by the passed integer
	 */
	String getTypeName(int type);

	/**
	 * Gets the value of the type represented by the passed name
	 */
	int getTypeValue(String type);

	/**
	 * This method will be called when the Connection's daemon receives a message.
	 * @param connection The connection that received the message
	 * @param message The message that was received
	 */
	void handleMessage(Connection connection, Message message);

	/**
	 * This method will be called when the Connection's daemon gets an exception while receiving a message.
	 * @param connection The connection that received the exception
	 * @param exception The exception thrown
	 */
	void handleException(Connection connection, Exception exception);
}
