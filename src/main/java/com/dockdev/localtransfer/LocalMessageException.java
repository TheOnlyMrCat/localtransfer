package com.dockdev.localtransfer;

import java.io.IOException;

public class LocalMessageException extends IOException {

	public LocalMessageException() {}

	public LocalMessageException(String message) {
		super(message);
	}

	public LocalMessageException(String message, Throwable cause) {
		super(message, cause);
	}

	public LocalMessageException(Throwable cause) {
		super(cause);
	}
}
